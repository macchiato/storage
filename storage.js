class Session {
  getItem(k) {
    return sessionStorage.getItem(k)
  }

  setItem(k, v) {
    sessionStorage.setItem(k, v)
  }

  removeItem(k) {
    sessionStorage.removeItem(k)
  }

  keys() {
    return Object.keys(sessionStorage)
  }

  values() {
    return Object.values(sessionStorage)
  }

  entries() {
    return Object.entries(sessionStorage)
  }
}

export class Storage {
  constructor() {
    this.session = new Session()
    this.plugins = []
  }

  addPlugin(plugin) {
    this.plugins.push(plugin)
  }

  getItem(k) {
    for (const plugin of this.plugins) {
      const result = plugin(k)
      if (result ?? undefined !== undefined) {
        return result
      }
    }
    return localStorage.getItem(k)
  }

  setItem(k, v) {
    localStorage.setItem(k, v)
  }

  removeItem(k) {
    localStorage.removeItem(k)
  }

  keys() {
    return Object.keys(localStorage)
  }

  values() {
    return Object.values(localStorage)
  }

  entries() {
    return Object.entries(localStorage)
  }
}