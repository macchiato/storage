class Session {
  constructor() {
    this.data = {}
  }

  getItem(k) {
    return this.data[k] ?? null
  }

  setItem(k, v) {
    this.data[k] = v
  }

  removeItem(k) {
    delete this.data[k]
  }

  keys() {
    return Object.keys(this.data)
  }

  values() {
    return Object.values(this.data)
  }

  entries() {
    return Object.entries(this.data)
  }
}

export class Storage {
  constructor(data = {}, sessionData = {}) {
    this.data = data
    this.session = new Session(sessionData)
  }

  getItem(k) {
    return this.data[k] ?? null
  }

  setItem(k, v) {
    this.data[k] = v
  }

  removeItem(k) {
    delete this.data[k]
  }

  keys() {
    return Object.keys(this.data)
  }

  values() {
    return Object.values(this.data)
  }

  entries() {
    return Object.entries(this.data)
  }
}